Rails.application.routes.draw do
  
  resources :people do
    collection do
      get :search
      post :search_result
    end
  end

  get ':controller(/:action(/:id(.:format)))'
  
  root to: 'people#index'

end
